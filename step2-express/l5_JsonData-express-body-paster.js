const express = require("express");
const path = require("path");
const app = express();

// config path
app.use("/public", express.static(path.join(__dirname, "static")));

// use body-parser
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "static", "index.html"));
});

app.post("/", (req, res) => {
  console.log(req.body);
  // some database call here
  res.json({ success: true });
});

app.listen(3000);
