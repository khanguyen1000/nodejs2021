const express = require("express");
const path = require("path");
const app = express();
// config static path
app.use("/public", express.static(path.join(__dirname, "static")));

// using body parser
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

// config template
app.set("view engine", "ejs");

const people = require("./routes/people");

app.use("/people", people);

// get home page
app.get("/", (req, res) => {
  res.render("index-l9");
});

app.listen(3000);
