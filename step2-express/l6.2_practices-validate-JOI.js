const Joi = require("joi");

const arrayString = ["banana", "bacon", "cheese"];
const arrayObjects = [{ example: "example1" }, { example: "example2" }];

const userInput = {
  personalInfo: {
    streetAddress: "123987987",
    city: "klasdksa",
    state: "fl",
  },
  preferences: arrayObjects,
};

const personalInfoSchema = Joi.object().keys({
  streetAddress: Joi.string().trim().required(),
  city: Joi.string().trim().required(),
  state: Joi.string().trim().length(2).required(),
});

// const preferencesSchema = Joi.array().items(Joi.string());
const preferencesSchema = Joi.array().items(
  Joi.object().keys({
    example: Joi.string().trim().required(),
  })
);

const schema = Joi.object().keys({
  personalInfo: personalInfoSchema,
  preferences: preferencesSchema,
});

const { value, error } = schema.validate(userInput);
if (error) {
  console.log(error);
} else {
  console.log(value);
}
