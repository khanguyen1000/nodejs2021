const express = require("express");
const path = require("path");
const app = express();

// config path
app.use("/public", express.static(path.join(__dirname, "static")));

// use body-parser
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.set("view engine", "ejs");
app.get("/:userQuery", (req, res) => {
  res.render("index", {
    data: {
      userQuery: req.params.userQuery,
      searchResults: ["book1", "book2", "book3"],
      loggedIn: true,
      username: "Kha Kha",
    },
  });
});

app.listen(3000);
