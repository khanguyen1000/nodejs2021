const express = require("express");
const app = express();
app.get("/", (req, res) => {
  res.send("Hello World");
});

app.get("/example", (req, res) => {
  res.send("hitting example route");
});

app.get("/example/:name/:age", (req, res) => {
  /**
   * params are using when you want route must have this params value
   * query is optional value , no need to have  ,
   *
   *  */

  console.log("params", req.params);
  console.log(req.query);
  res.send(req.params.name + " : " + req.params.age);
});

app.listen(3000);
