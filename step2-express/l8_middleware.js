const express = require("express");
const path = require("path");
const app = express();

// config static path file
app.use("/public", express.static(path.join(__dirname, "static")));
app.set("view engine", "ejs");
// using body-parser
app.use(express.urlencoded({ extended: false }));
app.use(express.json());
// using middleware
app.use((req, res, next) => {
  req.banana = "banana";
  next();
});
app.use("/example", (req, res, next) => {
  console.log(req.url, req.method);
  next();
});

app.get("/", (req, res) => {
  console.log(req.banana);
  res.render("index-l8");
});

app.get("/example", (req, res) => {
  res.send("Example page");
});

app.listen(3000);
