const express = require("express");
const path = require("path");
const Joi = require("joi");
const app = express();
// config path
app.use("/public", express.static(path.join(__dirname, "static")));
// use body-parser
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "static", "index.html"));
});

app.post("/", (req, res) => {
  console.log(req.body);
  const valueReq = {};
  req.body.forEach((item) => {
    valueReq[item.name] = item.value;
  });
  const schema = Joi.object().keys({
    email: Joi.string().trim().email().required(),
    password: Joi.string().min(5).max(10).required(),
  });
  const { value, error } = schema.validate(valueReq);
  console.log("value here:", value, "error here ", error);
  if (error) {
    console.log(error);
    res.send(error.details);
  }
  console.log(value);
  res.send("successfully posted data");
});

app.listen(3000);
