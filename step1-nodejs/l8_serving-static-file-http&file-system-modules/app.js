const http = require("http");
const fs = require("fs");
http
  .createServer((req, res) => {
    /**
     * Html file
     */
    // const readStreamHtml = fs.createReadStream("./static/index.html");
    // res.writeHead(200, { "Content-type": "text/html" });
    // readStreamHtml.pipe(res);

    /**
     * Json file
     */
    // const readStreamJson = fs.createReadStream("./static/example.json");
    // res.writeHead(200, { "Content-type": "application/json" });
    // readStreamJson.pipe(res);

    /**
     * image file
     */
    const readStreamImage = fs.createReadStream("./static/example.png");
    res.writeHead(200, { "Content-type": "image/png" });
    readStreamImage.pipe(res);
  })
  .listen(3000);
