const fs = require("fs");
const zlib = require("zlib");
const gzip = zlib.createGzip();
const gunzip = zlib.createGunzip();
// gzip file
// const readStream = fs.createReadStream("./example.txt", "utf8");
// const writeSteam = fs.createWriteStream("./example2.txt.gz");
// readStream.pipe(gzip).pipe(writeSteam);

// unzip file
const readStream = fs.createReadStream("./example2.txt.gz");
const writeSteam = fs.createWriteStream("./uncompressed.txt");
readStream.pipe(gunzip).pipe(writeSteam);
