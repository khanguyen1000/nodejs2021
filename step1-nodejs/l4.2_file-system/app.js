const fs = require("fs");
/**
 * create folder
 */
// fs.mkdir("tutorial", (err) => {
//   if (err) console.log(err);
//   else {
//     console.log("folder successfully created");
//   }
// });

/**
 * remove folder
 */
//  fs.rmdir("tutorial", (err) => {
//     if (err) console.log(err);
//     else {
//       console.log("Successfully deleted the folder");
//     }
//   });

// fs.mkdir("tutorial", (err) => {
//   if (err) console.log(err);
//   else {
//     fs.writeFile("./tutorial/example.txt", "123 example", (err) => {
//       if (err) console.log(err);
//       else {
//         console.log("Successfully created file");
//       }
//     });
//   }
// });

// remove dir function
// fs.unlink("./tutorial/example.txt", (err) => {
//   if (err) console.log(err);
//   else {
//     fs.rmdir("./tutorial", (err) => {
//       if (err) console.log(err);
//       else {
//         console.log("deleted folder");
//       }
//     });
//   }
// });

/**
 * delete multiple files
 *  */
// fs.readdir("./example", (err, files) => {
//   if (err) console.log(err);
//   else {
//     if (files.length > 0)
//       files.forEach((file, index) => {
//         fs.unlink(`./example/${file}`, (err) => {
//           if (err) console.log(err);
//           else console.log("successfully deleted file");
//         });
//       });
//   }
// });
