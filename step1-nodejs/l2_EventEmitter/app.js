const EventEmitter = require('events');
const eventEmitter = new EventEmitter();

eventEmitter.on('tutorial',(num1,num2)=>{
    console.log(num1+num2);
})

eventEmitter.emit('tutorial',1,2);


class Person extends EventEmitter{
    constructor(name,age){
        super();
        this._name = name;
        this._age = age;
    }
    get name(){
        return this._name;
    }
    get age(){
        return this._age;
    }
}

let pedro = new Person('Pedro',21);
let christina = new Person('Christina',17);
pedro.on('name',()=>{
    console.log(`my name is ${pedro.name}`);
})

pedro.on('age',()=>{
    console.log(`he is ${pedro.age}`);
})
christina.on('name',()=>{
    console.log(`my name is ${christina.name}`);
})
christina.on('age',()=>{
    console.log(`she is ${christina.age}`);
})

pedro.emit('name');
christina.emit('name');