const express = require("express");
const { ObjectId } = require("mongodb");
const path = require("path");
const app = express();

// body parser
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

const db = require("./db");
const collection = "todo";

const connectDB = () => {
  return db.getDB().collection(collection);
};

app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "index.html"));
});

app.get("/getTodos", (req, res) => {
  connectDB()
    .find({})
    .toArray((err, documents) => {
      if (err) console.log(err);
      else {
        console.log(documents);
        res.json(documents);
      }
    });
});

app.put("/:id", (req, res) => {
  const todoID = req.params.id;
  const userInput = req.body;
  console.log("userInput", userInput);
  connectDB().findOneAndUpdate(
    { _id: db.getPrimaryKey(todoID) },
    { $set: { todo: userInput.todo } },
    { upsert: true, returnNewDocument: true },
    (err, result) => {
      if (err) console.log(err);
      else {
        connectDB()
          .findOne(result.value._id)
          .then((documentFind) => {
            const value = { ...result, value: documentFind };
            console.log(value);
            res.json(value);
          });
      }
    }
  );
});

app.post("/", (req, res) => {
  const userInput = req.body;
  connectDB().insertOne(userInput, (err, result) => {
    if (err) console.log(err);
    else {
      connectDB()
        .findOne(result.insertedId)
        .then((documentFind) => {
          console.log("documentFind", documentFind);
          res.json({ status: result.acknowledged, document: documentFind });
        });
    }
  });
});

app.delete("/:id", (req, res) => {
  const todoID = req.params.id;

  connectDB().findOneAndDelete(
    { _id: db.getPrimaryKey(todoID) },
    (err, result) => {
      if (err) console.log(err);
      else {
        res.json(result);
      }
    }
  );
});

db.connect((err) => {
  if (err) {
    console.log("unable to connect to database");
    process.exit(1);
  } else {
    app.listen(3000, () => {
      console.log("connected to database, app listening on port 3000");
    });
  }
});
